#!/bin/bash
set -e

if [ "$1" = 'postgres' ]; then
	/etc/init.d/postgresql start
	echo "insert reviews"
	psql -h localhost -p 5432 -U onyme -d reviews -f "$2"
	echo "closed db connection"
	psql -h localhost -p 5432 -U onyme -d reviews
fi
exec "$@"

