### PostgreSQL 9.4 ###

*postgres image with [Amazon reviews](http://examples.citusdata.com/customer_reviews_nested_1998.json.gz) data in json/jsonb format*


run with endpoint script (data inserting):
```sh
$ docker run -t -i nidhal1925/postgres:latest
```

run directly in bash (without endpoint):
```sh
# run image
$ docker run -t -i nidhal1925/postgres:latest bin/bash
#start postgres service 
$ /etc/init.d/postgresql start
#connect to reviews db with role onyme
psql -h localhost -p 5432 -U onyme -d reviews -W
```
*db password: admin*